# ShopList

ShopList is a web application POC for saving shopping lists in a categorized user-friendly way.

## Development Stack

- Node.js
- Express.js
- MongoDB
- Angular

## Demo
![video demo for ShopList](https://media4.giphy.com/media/EO5WWJ8sXr63FMzYJL/giphy.gif)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)