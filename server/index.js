import express from 'express';
import routes from './routes/base.route'
import mongoose from 'mongoose'
import { dbUrl } from './config/Urls'

// config
const app = express();

// Middleware
app.use(express.static('public'));
app.use(express.json());
app.use((req, res, next) => {
    // Log each request to console
    console.log(`${req.method} Request from: ${req.originalUrl}`);

    // Enabling CORS [ONLY FOR TESTING PURPOSES REQUIRED BY THIS POC]
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
});

app.use((err, req, res, next) => {
    console.log(`ERROR: ${err.stack}`);
    res.status(500).send(`Error happened: ${err.stack} `);
    next();
});

// config db
mongoose.connect(dbUrl, {
    useNewUrlParser: true,
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// routes
app.use('/api', routes);

// startup
const port = process.env.PORT || '3000';
app.set('port', port);
app.listen(port, () => console.log(`listening on port ${port}`));