import mongoose from 'mongoose'

var item = new mongoose.Schema({
    title: String,
    isDone: Boolean
});

export const Item = mongoose.model('Item', item)