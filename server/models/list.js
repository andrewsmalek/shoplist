import mongoose from 'mongoose'

var list = new mongoose.Schema({
    title: String,
    items: [{
        title: String,
        isDone: Boolean
    }]
})

export const List = mongoose.model('List', list)