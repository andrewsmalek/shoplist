import { Router } from 'express';
import { List } from '../models/list';
import { Item } from '../models/item';

const router = Router();

router.get('/GetShoppingList', async(req, res) => {
    let data = await List.find();
    // simulate loading
    await new Promise(resolve => setTimeout(resolve, 1000));
    res.json(data);
});

router.post('/AddCategory', async(req, res) => {
    const model = new List(req.body)
    console.log(model.title);
    model.save();
    res.status(200).send();
});

router.post('/AddItem', async(req, res) => {
    const model = new Item({
        title: req.body.title,
        isDone: false
    });

    List.findOneAndUpdate({
        _id: req.body.categoryId
    }, {
        $push: {
            items: model
        }
    }, (error, success) => {
        if (error) {
            console.log(error);
            res.status(500).send();
        } else {
            console.log(success);
            res.status(200).send();
        }
    });
});

export default router;