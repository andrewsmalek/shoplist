import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { listDto } from '../models/list.dto';
import { BASE_URL } from '../utils/Url';
import { ItemRequest } from '../models/item.request';
import { ListRequest } from '../models/list.request';
import { IdRequest } from '../models/id.request';

@Injectable({
  providedIn: 'root'
})
export class ShopListService {
  constructor(private httpClient: HttpClient) { }

  getShoppingList() {
    return this.httpClient.get<Array<listDto>>(`${BASE_URL}/GetShoppingList`);
  }

  addCategory(listRequest: ListRequest) {
    return this.httpClient.post(`${BASE_URL}/AddCategory`, listRequest);
  }

  addItem(item: ItemRequest) {
    return this.httpClient.post(`${BASE_URL}/AddItem`, item);
  }

  toggleItemDone(item: IdRequest) {
    return this.httpClient.post(`${BASE_URL}/ToggleItemDone`, item);
  }
}
