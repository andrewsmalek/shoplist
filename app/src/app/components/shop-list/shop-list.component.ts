import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { IdRequest } from 'src/app/models/id.request';
import { ItemDto } from 'src/app/models/item.dto';
import { ItemRequest } from 'src/app/models/item.request';
import { listDto } from 'src/app/models/list.dto';
import { ListRequest } from 'src/app/models/list.request';
import { ShopListService } from 'src/app/services/shop-list.service';

@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(100, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(100, style({ opacity: 0 }))
      ])
    ])
  ]

})
export class ShopListComponent implements OnInit {
  isLoading = false;
  shoppingList: Array<listDto> = [];
  showCategoryContainer = false;
  showItemContainer = false;
  category = '';
  item = '';
  categoryId = '';

  constructor(private shopListService: ShopListService) { }

  ngOnInit(): void {
    this.getShoppingList();
  }

  toggleCategoryContainer() {
    this.category = '';
    this.showCategoryContainer = !this.showCategoryContainer;
  }

  toggleItemContainer(categoryId: string = '') {
    this.categoryId = categoryId;
    this.item = '';
    this.showItemContainer = !this.showItemContainer;
  }

  addCategory() {
    const model: ListRequest = {
      title: this.category
    };
    this.shopListService.addCategory(model)
      .subscribe(() => {
        this.toggleCategoryContainer();
        this.getShoppingList();
      });
  }

  addItem() {
    const model: ItemRequest = {
      title: this.item,
      categoryId: this.categoryId
    };
    this.shopListService.addItem(model)
      .subscribe(() => {
        this.toggleItemContainer();
        this.getShoppingList();
      });
  }

  toggleDone(item: ItemDto) {
    item.isDone = !item.isDone;
  }

  getShoppingList(): void {
    this.isLoading = true;
    this.shopListService.getShoppingList()
      .subscribe(res => {
        this.shoppingList = res;
        this.isLoading = false;
      });
  }
}
