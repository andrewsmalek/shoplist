export interface ItemDto {
    _id: string;
    title: string;
    isDone: boolean;
}