import { ItemDto } from "./item.dto";

export interface listDto {
    _id: string;
    title: string;
    items: ItemDto[];
}