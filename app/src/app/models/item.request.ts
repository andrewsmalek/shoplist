export interface ItemRequest {
    title: string;
    categoryId: string;
}